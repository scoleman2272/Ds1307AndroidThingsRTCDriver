/*
 * Copyright 2018 Scott Coleman.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.t2.ds1307;

import com.google.android.things.pio.I2cDevice;
import com.t2.ds1307.Ds1307.SquareWaveOutputFrequency;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.android.things.contrib.driver.testutils.BitsMatcher.hasBitsNotSet;
import static com.google.android.things.contrib.driver.testutils.BitsMatcher.hasBitsSet;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.byteThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;


public class Ds1307Test {

    @Mock
    private I2cDevice mI2c;

    @Rule
    public MockitoRule mMokitoRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException mExpectedException = ExpectedException.none();

    @Test
    public void close() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);
        ds1307.close();
        Mockito.verify(mI2c).close();
    }

    @Test
    public void close_safeToCallTwice() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);
        ds1307.close();
        ds1307.close(); // should not throw
        Mockito.verify(mI2c, times(1)).close();
    }

    @Test
    public void getTime() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);

        final Date dateResults = ds1307.getTime();
        Mockito.verify(mI2c).readRegBuffer(eq(0), any(byte[].class), eq(7));

    }

    @Test
    public void setTime() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);
        ds1307.close();
        mExpectedException.expect(IllegalStateException.class);
        final Date dateResults = ds1307.getTime();
    }

    @Test
    public void setTimeByDate() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);

//        String string = "January 2, 2010";
  //      DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        String string = "January 2, 2010 12:56:01 PM ";

        DateFormat format = new SimpleDateFormat ("MMMM d, yyyy hh:mm:ss a");
        Date thisDate = null;
        try {
            thisDate = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        byte[] buffer = new byte[7];

        buffer[0] = 1; // Calendar.SECOND));
        buffer[1] = 0x056; // Calendar.MINUTE));
        buffer[2] = 0x12; // Calendar.HOUR_OF_DAY));
//        buffer[2] &= // INDICATOR_12_HOURS);
        buffer[3] = 1;
        buffer[4] = 8; // Calendar.DAY_OF_MONTH));
        buffer[5] = 1;

        buffer[6] = 76;

        // Actual = [1, 86, 32, 7, 2, 0, 16]

        byte hour = decToPacketBcd(buffer[2]);

        ds1307.setTime(thisDate);
        Mockito.verify(mI2c).writeRegBuffer(eq(Ds1307.RTC_SECONDS_REG), eq(buffer), eq((byte)7));

    int i = 0;


    }
    private byte decToPacketBcd(int dec) {
        if (dec < 0 || dec > 99) {
            throw new IllegalArgumentException("dec must be between 0 and 99 included. dec: " + dec);
        }
        return (byte) (((dec / 10) << 4) + (dec % 10));
    }



    @Test
    public void getTime_throwsIfClosed() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);
        ds1307.close();
        mExpectedException.expect(IllegalStateException.class);
        final Date dateResults = ds1307.getTime();
    }




    @Test
    public void setOscillator() throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);

        ds1307.setOscillator(true);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG),
                byteThat(hasBitsNotSet((byte) Ds1307.RTC_CONTROL_REG_DISABLE_OSCILLATOR)));

        Mockito.reset(mI2c);
        ds1307.setOscillator(false);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG),
                byteThat(hasBitsSet((byte) Ds1307.RTC_CONTROL_REG_DISABLE_OSCILLATOR)));

    }

    @Test
    public void setSquareWave()  throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);

        ds1307.setSquareWave(true);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG),
                byteThat(hasBitsSet((byte) Ds1307.RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE)));

        Mockito.reset(mI2c);
        ds1307.setSquareWave(false);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG),
                byteThat(hasBitsNotSet((byte) Ds1307.RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE)));
    }

    @Test
    public void setSquareWaveOutputFrequency()  throws IOException {
        Ds1307 ds1307 = new Ds1307(mI2c);

        ds1307.setSquareWaveOutputFrequency(SquareWaveOutputFrequency.FREQUENCY_1_HZ);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG), eq((byte)0));
        Mockito.reset(mI2c);

        ds1307.setSquareWaveOutputFrequency(SquareWaveOutputFrequency.FREQUENCY_4096_HZ);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG), eq((byte)1));
        Mockito.reset(mI2c);

        ds1307.setSquareWaveOutputFrequency(SquareWaveOutputFrequency.FREQUENCY_8192_HZ);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG), eq((byte)2));
        Mockito.reset(mI2c);

        ds1307.setSquareWaveOutputFrequency(SquareWaveOutputFrequency.FREQUENCY_32786_HZ);
        Mockito.verify(mI2c).writeRegByte(eq(Ds1307.RTC_CONTROL_REG), eq((byte)3));
        Mockito.reset(mI2c);
    }


// Additional Tests--------
//different constructors
   // set time zone
// set time
        // min/max time
// et/set ytime in milli

// Square wave frequency
    // BDC routines
}
