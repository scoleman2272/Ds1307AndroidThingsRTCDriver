/*
 * Copyright 2018 Scott Coleman.
 * Copyright 2018 Roberto Leinardi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.t2.ds1307;

import android.support.annotation.IntDef;

import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManager;

import java.io.Closeable;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Driver for controlling the DS3231 real-time clock (RTC).
 */
@SuppressWarnings("WeakerAccess")
public class Ds1307 implements Closeable {
    private static final String TAG = Ds1307.class.getSimpleName();



    /**
     * I2C address for this peripheral
     *  @VisibleForTesting
     */
    public static final int I2C_ADDRESS = 0x68;
    public static final int TIMEKEEPING_INVALID = -1;

    public static final int TEMP_MEASUREMENT_INTERVAL_MS = (int) TimeUnit.SECONDS.toMillis(64);
    public static final int MIN_YEAR = 1900;
    public static final int MAX_YEAR = 2099;

     /** DS1307 I2C Control REgisters
     *  @VisibleForTesting
     */
     public static final int RTC_SECONDS_REG    = 0x00;
     public static final int RTC_MINUTES_REG    = 0x01;
     public static final int RTC_HOURS_REG      = 0x02;
     public static final int RTC_DAY_REG        = 0x03;
     public static final int RTC_DATE_REG       = 0x04;
     public static final int RTC_MONTH_REG      = 0x05;
     public static final int RTC_YEAR_REG       = 0x06;
     public static final int RTC_CONTROL_REG    = 0x07;

    /**
     * When high, the 12-hour mode is selected
     */
    private static final int INDICATOR_12_HOURS = 0b0100_0000;

    /**
     * In the 12-hour mode, is the AM/PM bit with logic-high being PM.
     */
    private static final int INDICATOR_PM       = 0b0010_0000;

    /**
     * When set to logic 0, the oscillator is started. When set to logic 1, the oscillator is stopped.
     * This bit is clear (logic 0) when power is first applied.
     *  @VisibleForTesting
     */
    public static final int RTC_CONTROL_REG_DISABLE_OSCILLATOR = 0b1000_0000;

    /**
     * When set to logic 1 this bit enables the square wave.
     *  @VisibleForTesting
     */
    public static final int RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE = 0b0001_0000;

    /**
     * See {@link #RTC_CONTROL_REG_RATE_SELECT1}.
     *  @VisibleForTesting
     */
    public static final int RTC_CONTROL_REG_RATE_SELECT2 = 0b0000_0010;

    /**
     * {@link #RTC_CONTROL_REG_RATE_SELECT1} and {@link #RTC_CONTROL_REG_RATE_SELECT2} control the frequency of the
     * square-wave
     * output when the square wave has been enabled. The following table shows the square-wave frequencies that can
     * be selected with the RS bits. These bits are both set to logic 1 (8.192kHz) when power is first applied.
     *  @VisibleForTesting
     */
    public static final int RTC_CONTROL_REG_RATE_SELECT1 = 0b0000_0001;

    /*
     * Time zone - default to UTC
     */
    private TimeZone mTimeZone = TimeZone.getTimeZone(ZoneOffset.UTC);

    /*
     * I2C device backing the RTC
     */
    private I2cDevice mI2cDevice;

    /**
     * Create a new Ds1307 sensor driver connected to the given I2c device.
     * @param device I2C device of the sensor.
     * @throws IOException
     */
    /*package*/  Ds1307(I2cDevice device) throws IOException {
        try {
            connect(device);
        } catch (IOException | RuntimeException e) {
            try {
                close();
            } catch (IOException | RuntimeException ignored) {
            }
            throw e;
        }
    }

    /**
     * Create a new Ds1307 driver connected to the named I2C bus
     *
     * @param i2cName I2C bus name the RTC is connected to
     * @throws IOException
     */
    public Ds1307(String i2cName) throws IOException {
        this(i2cName, I2C_ADDRESS);
    }

    /**
     * Create a new Ds1307 driver connected to the named I2C bus and address
     * with the given dimensions.
     *
     * @param i2cName    I2C bus name the RTC is connected to
     * @param i2cAddress I2C address of the RTC
     * @throws IOException
     */
    public Ds1307(String i2cName, int i2cAddress) throws IOException {

        List<String>  deviceList =  PeripheralManager.getInstance().getI2cBusList();

        I2cDevice device = PeripheralManager.getInstance().openI2cDevice(i2cName, i2cAddress);
        try {
            connect(device);
        } catch (IOException | RuntimeException e) {
            try {
                close();
            } catch (IOException | RuntimeException ignored) {
            }
            throw e;
        }
    }

    private void connect(I2cDevice device) throws IOException {
        mI2cDevice = device;
    }

    @Override
    public void close() throws IOException {
        if (mI2cDevice != null) {
            try {
                mI2cDevice.close();
            } finally {
                mI2cDevice = null;
            }
        }
    }

    public void setTimeZone(TimeZone zone) {
        mTimeZone = zone;
    }

    /**
     * Gets the Real-Time clock.
     *
     * @return a {@link Date} containing the Real-Time clock or null if the timekeeping data is not valid.
     * @throws IOException
     */
    public Date getTime() throws IOException {

            byte[] buffer = new byte[7];
            readRegBuffer(RTC_SECONDS_REG, buffer, buffer.length);

            int second = packetBcdToDec(buffer[0]);
            int minute = packetBcdToDec(buffer[1]);
            int hour = getHourFromBcd(buffer[2]);
            int day = packetBcdToDec(buffer[4]);
            int month = packetBcdToDec((byte) (buffer[5] & 0b0001_1111));
            int year = 2000 + packetBcdToDec(buffer[6]);

            Calendar calendar = Calendar.getInstance(mTimeZone);
            calendar.set(year, month, day, hour, minute, second);

            return calendar.getTime();
    }

    /**
     * Sets the Real-Time clock time and calendar data.
     *
     * @param timeInMillis the time and calendar data you want to set in milliseconds.
     * @throws IOException
     */
    public void setTime(long timeInMillis) throws IOException {
        setTime(new Date(timeInMillis));
    }

    /**
     * Gets the Real-Time clock in milliseconds.
     *
     * @return a {@link Date} containing the Real-Time clock in milliseconds or {@link #TIMEKEEPING_INVALID} if the
     * timekeeping data is not valid.
     * @throws IOException
     */
    public long getTimeInMillis() throws IOException {
        Date date = getTime();
        if (date != null) {
            return date.getTime();
        } else {
            return TIMEKEEPING_INVALID;
        }
    }

    /**
     * Sets the Real-Time clock time and calendar data.
     *
     * @param date the time and calendar data you want to set.
     * @throws IOException
     */
    public void setTime(Date date) throws IOException {
        Calendar calendar = Calendar.getInstance(mTimeZone);
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        if (year < MIN_YEAR || year > MAX_YEAR) {
            throw new IllegalArgumentException("Year must be between 1900 and 2099 included. Year: " + year);
        }
        byte[] buffer = new byte[7];

        buffer[0] = decToPacketBcd(calendar.get(Calendar.SECOND));
        buffer[1] = decToPacketBcd(calendar.get(Calendar.MINUTE));
        buffer[2] = decToPacketBcd(calendar.get(Calendar.HOUR_OF_DAY));
        buffer[2] &= ~(INDICATOR_12_HOURS);
        buffer[3] = decToPacketBcd(calendar.get(Calendar.DAY_OF_WEEK));
        buffer[4] = decToPacketBcd(calendar.get(Calendar.DAY_OF_MONTH));
        buffer[5] = decToPacketBcd(calendar.get(Calendar.MONTH));

        buffer[6] = decToPacketBcd(year % 100);

        setOscillator(true);

        writeRegBuffer(RTC_SECONDS_REG, buffer, buffer.length);
    }

    /**
     * Current status of the oscillator
     *
     * @return True if enabled, false if disabled.
     * @throws IOException
     */
    public boolean isOscillatorEnabled() throws IOException {
        return ((readRegByte(RTC_CONTROL_REG) & 0xFF) & RTC_CONTROL_REG_DISABLE_OSCILLATOR)
                != RTC_CONTROL_REG_DISABLE_OSCILLATOR;
    }

    /**
     * Starts, stops the oscillator.
     * <p>
     * It is set to true when power is first applied. When the Ds1307 is powered by V CC, the
     * oscillator is always on regardless of the value send. When disabled, all register data is static.
     *
     * @param enabled When set to true, the oscillator is started. When set to false, the oscillator is stopped when
     *                the Ds1307 switches to V BAT.
     * @throws IOException
     */
    public void setOscillator(boolean enabled) throws IOException {
        byte reg = readRegByte(RTC_CONTROL_REG);
        reg &= ~(RTC_CONTROL_REG_DISABLE_OSCILLATOR);
        if (!enabled) {
            reg |= RTC_CONTROL_REG_DISABLE_OSCILLATOR;
        }
        writeRegByte(RTC_CONTROL_REG, reg);
    }

    /**
     * Current status of the square wave
     *
     * @return True if enabled, false if disabled.
     * @throws IOException
     */
    public boolean isSquareWaveEnabled() throws IOException {
        return ((readRegByte(RTC_CONTROL_REG) & 0xFF) & RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE)
                == RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE;
    }

    /**
     * Enable/disable the square wave.
     * It is set to false when power is first applied.
     *
     * @param enabled When set to true it enables the square wave.
     * @throws IOException
     */
    public void setSquareWave(boolean enabled) throws IOException {
        byte reg = readRegByte(RTC_CONTROL_REG);
        reg &= ~(RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE);
        if (enabled) {
            reg |= RTC_CONTROL_REG_BATTERY_BACKED_SQUARE_WAVE_ENABLE;
        }
        writeRegByte(RTC_CONTROL_REG, reg);
    }

    /**
     * Gets the current square-wave output frequency.
     *
     * @return
     * @throws IOException
     */
    public int getSquareWaveOutputFrequency() throws IOException {
        return (readRegByte(RTC_CONTROL_REG) & 0xFF) & SquareWaveOutputFrequency.FREQUENCY_32786_HZ;
    }

    /**
     * Sets the frequency of the square-wave output when the square wave has been enabled.
     *
     * @param squareWaveOutputFrequency one of the values of {@link SquareWaveOutputFrequency}.
     * @throws IOException
     */
    public void setSquareWaveOutputFrequency(@SquareWaveOutputFrequency int squareWaveOutputFrequency) throws
            IOException {
        byte reg = readRegByte(RTC_CONTROL_REG);
        reg &= ~(SquareWaveOutputFrequency.FREQUENCY_32786_HZ);
        reg |= squareWaveOutputFrequency;
        writeRegByte(RTC_CONTROL_REG, reg);
    }

    /**
     * Read a byte from a given register.
     *
     * @param reg The register to read from (0x00-0xFF).
     * @return The value read from the device.
     * @throws IOException
     */
    private byte readRegByte(int reg) throws IOException {
        if (mI2cDevice == null) {
            throw new IllegalStateException("I2C device not open");
        }
        return mI2cDevice.readRegByte(reg);
    }

    /**
     * Read multiple bytes from a given register.
     *
     * @param reg    The register to read from (0x00-0xFF).
     * @param buffer Buffer to read data into.
     * @param length Number of bytes to read, may not be larger than the buffer size.
     * @throws IOException
     */
    private void readRegBuffer(int reg, byte[] buffer, int length) throws IOException {
        if (mI2cDevice == null) {
            throw new IllegalStateException("I2C device not open");
        }
        mI2cDevice.readRegBuffer(reg, buffer, length);
    }

    /**
     * Write a byte to a given register.
     *
     * @param reg The register to write to (0x00-0xFF).
     * @throws IOException
     */
    private void writeRegByte(int reg, byte data) throws IOException {
        if (mI2cDevice == null) {
            throw new IllegalStateException("I2C device not open");
        }
        mI2cDevice.writeRegByte(reg, data);
    }

    /**
     * Write a byte array to a given register.
     *
     * @param reg    The register to write to (0x00-0xFF).
     * @param buffer Data to write.
     * @param length Number of bytes to write, may not be larger than the buffer size.
     * @throws IOException
     */
    private void writeRegBuffer(int reg, byte[] buffer, int length) throws IOException {
        if (mI2cDevice == null) {
            throw new IllegalStateException("I2C device not open");
        }
        mI2cDevice.writeRegBuffer(reg, buffer, length);
    }

    private int getHourFromBcd(byte bcd) {
        int hour;
        if (((bcd & 0xFF) & INDICATOR_12_HOURS) == INDICATOR_12_HOURS) {
            hour = packetBcdToDec((byte) ((bcd & 0xFF) & ~(INDICATOR_12_HOURS | INDICATOR_PM)));
            if (((bcd & 0xFF) & INDICATOR_PM) == INDICATOR_PM) {
                hour += 12;
            }
        } else {
            hour = packetBcdToDec((byte) ((bcd & 0xFF) & ~(INDICATOR_12_HOURS)));
        }
        return hour;
    }

    private int packetBcdToDec(byte bcd) {
        return (((bcd & 0xFF) >> 4) * 10) + (bcd & 0xF);
    }

    private byte decToPacketBcd(int dec) {
        if (dec < 0 || dec > 99) {
            throw new IllegalArgumentException("dec must be between 0 and 99 included. dec: " + dec);
        }
        return (byte) (((dec / 10) << 4) + (dec % 10));
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SquareWaveOutputFrequency.FREQUENCY_1_HZ,
            SquareWaveOutputFrequency.FREQUENCY_4096_HZ,
            SquareWaveOutputFrequency.FREQUENCY_8192_HZ,
            SquareWaveOutputFrequency.FREQUENCY_32786_HZ
    })
    public @interface SquareWaveOutputFrequency {
        int FREQUENCY_1_HZ      = 0b0000_0000;
        int FREQUENCY_4096_HZ   = 0b0000_0001;
        int FREQUENCY_8192_HZ   = 0b0000_0010;
        int FREQUENCY_32786_HZ  = 0b0000_0011;
    }
}
