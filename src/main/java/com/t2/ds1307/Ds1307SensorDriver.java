/*
 * Copyright 2018 Scott Coleman.
 * Copyright 2018 Roberto Leinardi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.t2.ds1307;

import android.hardware.Sensor;

import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class Ds1307SensorDriver implements AutoCloseable {
    // DRIVER parameters
    // documented at https://source.android.com/devices/sensors/hal-interface.html#sensor_t
    private static final String DRIVER_VENDOR = "Maxim Integrated Products";
    private static final String DRIVER_NAME = "DS1307";

    private static final int DRIVER_VERSION = 1;

    private Ds1307 mDevice;

    /**
     * Create a new framework sensor driver connected on the given bus and address.
     * The driver emits {@link Sensor} with temperature data when registered.
     *
     * @param bus I2C bus the sensor is connected to.
     * @throws IOException
     */
    public Ds1307SensorDriver(String bus) throws IOException {
        this(bus, Ds1307.I2C_ADDRESS);
    }

    /**
     * Create a new framework sensor driver connected on the given bus and address.
     * The driver emits {@link Sensor} with temperature data when registered.
     *
     * @param bus        I2C bus the sensor is connected to.
     * @param i2cAddress I2C address of the RTC
     * @throws IOException
     */
    public Ds1307SensorDriver(String bus, int i2cAddress) throws IOException {
        mDevice = new Ds1307(bus, i2cAddress);
    }

    /**
     * Close the driver and the underlying device.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        if (mDevice != null) {
            try {
                mDevice.close();
            } finally {
                mDevice = null;
            }
        }
    }
}
